﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamControler : MonoBehaviour
{
    [SerializeField]
    private Transform targetToFollow;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        transform.position = new Vector3(
             Mathf.Clamp(targetToFollow.position.x, 1.0f, 45.0f),
             Mathf.Clamp(targetToFollow.position.y, 4.0f, 5.0f),
             transform.position.z);

    }
}
