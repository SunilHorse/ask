﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Flip : MonoBehaviour
{
    public float inputX;
    public float inputY;
    bool facingRight = true;
    public float moveSpeed = 5;

    Animator anim;
    Rigidbody2D rb;
    SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();


    }

    private void Update()
    {
        inputX = Input.GetAxis("Horizontal");
        inputY = Input.GetAxis("Vertical");

        transform.position = new Vector2(
         Mathf.Clamp(transform.position.x, -7.0f, 52.0f),
         Mathf.Clamp(transform.position.y, 2.3f, 7.0f));

    }
    // Update is called once per frame
    void FixedUpdate()
    {
        // flip the Srpite 

        if (inputX < 0 && facingRight)
        {
            flip();
        }
        else if (inputX > 0 && !facingRight)
        {
            flip();
        }
    }


    void flip()
    {
        facingRight = !facingRight;

        transform.Rotate(Vector3.up, -180f, 0f);
    }

}
