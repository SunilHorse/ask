﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerA : MonoBehaviour
{

    public GameObject JumpPower;
    
    private void OnTriggerEnter2D(Collider2D collider)
    {
        JumpPower.GetComponent<TwiceJump>().enabled = true;

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
