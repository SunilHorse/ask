﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Run : MonoBehaviour
{

    [SerializeField]
    private LayerMask platformsLayerMask;
    private Rigidbody2D rb2d;
    float jumpVelocity = 10.0f;
    private BoxCollider2D boxCollider2d;
    float moveSpeed = 5.0f;
    Animator anim;
   
    // Start is called before the first frame update
    void Start()
    {




    }

    private void Awake()
    {
        rb2d = transform.GetComponent<Rigidbody2D>();
        boxCollider2d = transform.GetComponent<BoxCollider2D>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {


        if (IsGrounded() && Input.GetKeyDown(KeyCode.Space))
        {
            rb2d.velocity = Vector2.up * jumpVelocity;

        }

        Movement();

        // animation set

        if (IsGrounded())
        {
            if (rb2d.velocity.x == 0)
            {
                anim.Play("Idle_Animation");
            }
            else
            {
                anim.Play("Run_Animation");
            }
        }
        if (!IsGrounded())
        {
            anim.Play("Jump_Animation");
        }

    }



    private bool IsGrounded()
    {


        RaycastHit2D raycastHit2d = Physics2D.BoxCast(boxCollider2d.bounds.center, boxCollider2d.bounds.size, 0f, Vector2.down, .1f, platformsLayerMask);
        Debug.Log(raycastHit2d.collider);
        return raycastHit2d.collider != null;
    }
    private void Movement()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb2d.velocity = new Vector2(-moveSpeed, rb2d.velocity.y);

        }
        else
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                rb2d.velocity = new Vector2(moveSpeed, rb2d.velocity.y);

            }
            else
            {
                rb2d.velocity = new Vector2(0, rb2d.velocity.y);


            }
        }

    }
}
