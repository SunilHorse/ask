﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwiceJump : MonoBehaviour
{
    private Rigidbody2D rb2d;
    float jumpVelocity = 12.0f;
    float moveSpeed = 5.0f;
    private BoxCollider2D boxCollider2d;
    Animator anim;
    [SerializeField]
    private LayerMask platformsLayerMask;
    private bool doubleJump = true;



    // Start is called before the first frame update
    void Start()
    {
        rb2d = transform.GetComponent<Rigidbody2D>();
        boxCollider2d = transform.GetComponent<BoxCollider2D>();
        anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()

    {
        DoubleJump();

    }

    void DoubleJump()
    {
        if (IsGrounded())
        {
            doubleJump = true;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (IsGrounded())
            {
                rb2d.velocity = Vector2.up * jumpVelocity;
            }
            else
            {
                if (doubleJump)
                {
                    rb2d.velocity = Vector2.up * jumpVelocity;
                    doubleJump = false;
                }
            }
        }
    }

    private bool IsGrounded()
    {


        RaycastHit2D raycastHit2d = Physics2D.BoxCast(boxCollider2d.bounds.center, boxCollider2d.bounds.size, 0f, Vector2.down, .1f, platformsLayerMask);
        Debug.Log(raycastHit2d.collider);
        return raycastHit2d.collider != null;
    }


}
